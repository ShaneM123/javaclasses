public class Cylinder extends circleBase implements Volumes, Comparable<Volumes>{
    private double height;

    public Cylinder(){
		this(1,1);
    }
    public Cylinder(int radius, int height){
        super(radius);
        this.height=(double)height;
    }

    public Cylinder(String colour, double height) {
        super(colour);
        this.height = height;
    }

    public Cylinder(String colour, int radius, double height) {
        super(colour, radius);
        this.height = height;
    }

    public Cylinder(String colour, double radius, double height) {
        super(colour, radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    double calcSurfaceArea() {
        return (2*Math.PI*radius*height)+(2*Math.PI*(radius*radius));
    }


    @Override
    public double volumeCalc() {
        double volume;
        volume= Math.PI * radius * height;
        return volume;
    }

    @Override
    public int compareTo(Volumes input){

        return Double.compare(volumeCalc(), input.volumeCalc());
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "height=" + height +
                ", radius=" + radius +
                ", Volume=" +volumeCalc() +
                '}';
    }
}

