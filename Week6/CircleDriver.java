import java.util.Arrays;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class CircleDriver extends JFrame {


	public static void main(String[] args) {



		circleBase[] shapes = new circleBase[5];
		String input;
		String result = "";
		int validInput = 0;
		int validRad = 0;
		int validHeight = -1;
		String input2 = "";
		String result2 = "";
		String validAns = "";
		boolean correct = false;

		for (int i = 0; i < shapes.length; i++) {


			do {
				try {
					validInput = 0;
					input = JOptionPane.showInputDialog(null, "select if you want a cylinder(type\"1\") or a cone(type \"2\")");
					validInput = Integer.parseInt(input);

				} catch (NumberFormatException anError) {
					JOptionPane.showMessageDialog(null, "Error (please re-try): " + anError.getMessage());
					validInput = 0;
				}
				if (validInput == 1 || validInput == 2) {
					correct = true;
				}
			} while (!correct);


			if (validInput == 1) {
				result = "you selected Cylinder";
				JOptionPane.showMessageDialog(null, result);

				try {
					input = JOptionPane.showInputDialog(null, "would you like to specify height and radius of your Cylinder, y/n?");
					validAns = input.toLowerCase();
				} catch (Exception anError) {
					JOptionPane.showMessageDialog(null, "Error " + anError.getMessage());
				}

				if (validAns.equals("y")) {

					do {
						try {
							input2 = JOptionPane.showInputDialog(null, "please select a height of your Cylinder");
							validHeight = Integer.parseInt(input2);

						} catch (NumberFormatException anError) {
							JOptionPane.showMessageDialog(null, "Error (please re-try): " + anError.getMessage());
							validHeight = -1;
						}
					} while (validHeight < 0);

					do {
						try {
							input2 = JOptionPane.showInputDialog(null, "please select a radius of your Cylinder");
							validRad = Integer.parseInt(input2);

						} catch (NumberFormatException anError) {
							JOptionPane.showMessageDialog(null, "Error (please re-try): " + anError.getMessage());
							validRad = -1;
						}
					} while (validRad < 0);


					shapes[i] = new Cylinder(validRad, validHeight);
				} else {
					shapes[i] = new Cylinder();
				}
			}


			if (validInput == 2) {
				result = "you selected Cone";
				JOptionPane.showMessageDialog(null, result);

				try {
					input = JOptionPane.showInputDialog(null, "would you like to specify height and radius of your Cone, y/n?");
					validAns = input.toLowerCase();
				} catch (Exception anError) {
					JOptionPane.showMessageDialog(null, "Error " + anError.getMessage());
				}

				if (validAns.equals("y")) {

					do {
						try {
							input2 = JOptionPane.showInputDialog(null, "please select a height of your Cone");
							validHeight = Integer.parseInt(input2);

						} catch (NumberFormatException anError) {
							JOptionPane.showMessageDialog(null, "Error (please re-try): " + anError.getMessage());
							validHeight = -1;
						}
					} while (validHeight < 0);

					do {
						try {
							input2 = JOptionPane.showInputDialog(null, "please select a radius of your Cone");
							validRad = Integer.parseInt(input2);

						} catch (NumberFormatException anError) {
							JOptionPane.showMessageDialog(null, "Error (please re-try): " + anError.getMessage());
							validRad = -1;
						}
					} while (validRad < 0);

					shapes[i] = new Cone(validRad, validHeight);
				} else {
					shapes[i] = new Cone();
				}

			}

		}
		Arrays.sort(shapes);
		for (int i = 0; i < shapes.length; i++) {
			System.out.println(shapes[i].toString());
		}


	}


}