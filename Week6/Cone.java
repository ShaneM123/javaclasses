public class Cone extends  circleBase implements Volumes, Comparable<Volumes>{
    private double height;


    public Cone(){
        this(1,1);
    }
    public Cone(int radius, int height){
        super(radius);
        this.height=(double)height;

    }

    public Cone(String colour, double height) {
        super(colour);
        this.height = height;
    }

    public Cone(String colour, int radius, double height) {
        super(colour, radius);
        this.height = height;
    }

    public Cone(String colour, double radius, double height) {
        super(colour, radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    double calcSurfaceArea() {
        return Math.PI * radius*(radius+Math.sqrt(((height*height)+(radius*radius))));
    }


    @Override
     public double volumeCalc() {
         double volume;
        volume= Math.PI * radius * (height/3);
        return volume;
    }

    @Override
    public int compareTo(Volumes input){

        return Double.compare(volumeCalc(), input.volumeCalc());
    }

    @Override
    public String toString() {
        return "Cone{" +
                "height=" + height +
                ", volume=" + volumeCalc() +
                ", radius=" + radius +
                '}';
    }
}


