import java.awt.FlowLayout;

import javax.swing.JOptionPane;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

public class SwingJButtonRadioTest extends  JFrame {

    public SwingJButtonRadioTest(){
        super("Swing JButton");
        JRadioButton option1 = new JRadioButton("Cone");
        JRadioButton option2 = new JRadioButton("Cylinder");

        ButtonGroup group = new ButtonGroup();
        group.add(option1);
        group.add(option2);

        setLayout(new FlowLayout());

        add(option1);
        add(option2);

        pack();
    }

}
