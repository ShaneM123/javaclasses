public abstract class circleBase {
    private String colour;
    protected double radius;

    public circleBase() {
        this.colour = "No Colour";
        this.radius = 1;
    }
    public circleBase(int radius) {
        this.colour = "No Colour";
        this.radius = (double) radius;
    }
    public circleBase(String colour) {
        this.colour = colour;
        this.radius = 1;
    }
    public circleBase(String colour, int radius) {
        this.colour = colour;
        this.radius = (double) radius;
    }
    public circleBase(String colour, double radius) {
        this.colour = colour;
        this.radius = radius;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    abstract double calcSurfaceArea();

}
