package com.shanes.company;
import java.text.DecimalFormat;

/**
   A product in a vending machine.
*/
public class Product
{  
   private String description;
   private double price;
   private int quantity;
   DecimalFormat format = new DecimalFormat(".00");

   /**
      Constructs a Product object
      @param aDescription the description of the product
      @param aPrice the price of the product
   */
   public Product(String aDescription, double aPrice, int quantity)
   {  
      description = aDescription;
      price = aPrice;
      this.quantity= quantity;
   }

   public void setPrice(double price) {
      this.price = price;
   }

   public void setQuantity(int quantity) {
      this.quantity = quantity;
   }
   public String getQuantityAsString(){
       String strQuantity= "" + getQuantity();
      return strQuantity;
   }

   public int getQuantity() {
      return quantity;
   }

   /**
      Gets the description.
      @return the description
   */
   public String getDescription()
   { 
      return description;
   }
   
   /**
      Gets the price.
      @return the price
   */
   public double getPrice()
   {  
      return price;
   }

   /**
      Determines of this product is the same as the other product.
      @param other the other product
      @return true if the theVendingMachine are equal, false otherwise
   */
   public boolean equals(Object other)
   { 
      if (other == null) return false;
      Product b = (Product) other;
      return description.equals(b.description) && price == b.price;
   }

   /**
      Formats the product's description and price.
   */
   public String toString()
   {
      return description + ", "+ format.format(price) + ", " + quantity+" END";
   }

}
