package com.shanes.company;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class ProductListGui {

    private theVendingMachine vend;
    public ProductListGui(theVendingMachine vend) {
        this.vend= vend;
    }

    public  void start(Stage window) throws Exception {

        PurchaseGui purchasegui = new PurchaseGui(vend);
        window.setTitle("Vending Machine");
        GridPane pane = new GridPane();

        pane.setPadding(new Insets(10,10,10,10));
        pane.setHgap(10);
        pane.setVgap(8);
        pane.setAlignment(Pos.CENTER);
       String products = vend.altProductNameList();
        Label thelabel = new Label();
        thelabel.setText(products);

        GridPane.setConstraints(thelabel,0,0,2,1);
        Button backButton = new Button ("<-Back");
        GridPane.setConstraints(backButton,0,2);
        Button buyButton = new Button ("Purchase->");
        GridPane.setConstraints(buyButton,1,2);
        UserGui usergui = new UserGui(vend);

        backButton.setOnAction(e->   {try{
            usergui.start(window); }
        catch (Exception d){
            System.out.println(d.getMessage());}});

        buyButton.setOnAction(e-> {try{
            purchasegui.start(window);}
        catch(Exception c){
            System.out.println(c.getMessage());
        }
        });

        pane.getChildren().addAll(backButton,thelabel,buyButton);
        Scene scene = new Scene(pane,500, 300);
        window.setScene(scene);
        window.show();
    }
}

