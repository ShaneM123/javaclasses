package com.shanes.company;
import java.text.DecimalFormat;

public class Account{
    private String acName;
    private  String password;
    DecimalFormat format = new DecimalFormat(".00");

        public Account(){
            acName="Default";
            password="Default";
        }

    public Account(String acName, String password) {
        this.acName = acName;
        this.password = password;

    }

    public String getAcName(){return acName;}
    public String getPassword(){return password;}


    @Override
    public String toString() {
        return "Name: " + acName + " Password: "+ getPassword();
    }

}