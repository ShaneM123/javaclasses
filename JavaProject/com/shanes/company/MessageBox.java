package com.shanes.company;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MessageBox {

    public static void display(String title, String message){
        Stage window = new Stage();
        VBox layout = new VBox(10);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);
        Label label = new Label();
        label.setText(message);
        Button yesButton = new Button("Okay");
        yesButton.setOnAction(event -> {
            window.close();
        });
        layout.getChildren().addAll(label, yesButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene =new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
    }
}
