package com.shanes.company;
import com.shanes.company.AdminGui;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class AdminProductListGui {

    private theVendingMachine vend;
    public AdminProductListGui(theVendingMachine vend) {
        this.vend= vend;
    }

    public  void start(Stage window) throws Exception {

        AdminGui adminGui = new AdminGui(vend);
        window.setTitle("Vending Machine");
        GridPane pane = new GridPane();

        pane.setPadding(new Insets(10,10,10,10));
        pane.setHgap(10);
        pane.setVgap(8);
        pane.setAlignment(Pos.CENTER);
        String products = vend.altProductNameList();
        Label thelabel = new Label();
        thelabel.setText(products);

        GridPane.setConstraints(thelabel,0,0,2,1);
        Button backButton = new Button ("<-Back");
        GridPane.setConstraints(backButton,0,2);
        UserGui usergui = new UserGui(vend);

        backButton.setOnAction(e->   {try{
            adminGui.start(window); }
        catch (Exception d){
            System.out.println(d.getMessage());}});


        pane.getChildren().addAll(backButton,thelabel);
        Scene scene = new Scene(pane,500, 300);
        window.setScene(scene);
        window.show();
    }
}

