package com.shanes.company;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class theVendingMachine {
    private ArrayList<Product> product = new ArrayList<>();
    private ArrayList<Product> emptyProduct = new ArrayList<>();
    private ArrayList<UserAccount> accounts = new ArrayList<>();
    private ArrayList<AdminAccount> admins = new ArrayList<>();
    StringBuffer stringBufferOfData = new StringBuffer();
    StringBuffer stringBufferOfProducts = new StringBuffer();
    StringBuffer adminBufferData = new StringBuffer();
    FileHandler handler = new FileHandler();
    String theFile2 = "./Accounts.dat";
    String theFile1 = "./products.dat";
    String theFile3 = "./Admins.dat";
    private Account theAc;
    String test = "";
    File g = new File("Accounts.dat");
    File h = new File("Admins.dat");
    int i = 0;

    public ArrayList<UserAccount> getAccounts() {
        return accounts;
    }

    public Account getTheAc() {
        return theAc;
    }

    public boolean isAdmin() {
        if (theAc instanceof AdminAccount) {
            return true;
        } else return false;
    }

    public int sizeOfProductList() {
        return product.size();
    }

    public void readProducts() throws Exception {
        ArrayList<String> handledArray = readIn(theFile1);
        for (i = 0; i < handledArray.size(); i++) {
            stringBufferOfProducts.append(handledArray.get(i)).append("\r\n");
            product.add(productAdder(handledArray.get(i)));
            if (productAdder(handledArray.get(i)).getQuantity() == 0) {
                emptyProduct.add(productAdder(handledArray.get(i)));
            }
        }
        handledArray.clear();
    }

    public ArrayList<String> readIn(String theFile) throws Exception {
        return new ArrayList<>(handler.fileReader(theFile));
    }

    public void readAccounts() throws Exception {
        ArrayList<String> handledAcArray = new ArrayList<>(readIn(theFile2));
        for (i = 0; i < handledAcArray.size(); i++) {
            stringBufferOfData.append(handledAcArray.get(i)).append("\r\n");
            accounts.add(userAcParser(handledAcArray.get(i)));
        }
    }

    public void readAdmin() throws Exception {
        ArrayList<String> handledAcArray = new ArrayList<>(readIn(theFile3));
        for (i = 0; i < handledAcArray.size(); i++) {
            adminBufferData.append(handledAcArray.get(i)).append("\r\n");
            admins.add(adminAcParser(handledAcArray.get(i)));
        }
    }

    public boolean acChecker(String name) {
        for (int i = 0; i < accounts.size(); i++) {
            if (name.equalsIgnoreCase(accounts.get(i).getAcName())) {
                theAc = accounts.get(i);
                return true;
            }
        }
        return acAdminChecker(name);

    }

    public boolean guiAcChecker(String name) {
        for (int i = 0; i < accounts.size(); i++) {
            if (name.equalsIgnoreCase(accounts.get(i).getAcName())) {
                theAc = accounts.get(i);
                return true;
            }
        }
        return false;
    }

    public boolean acAdminChecker(String name) {
        for (int i = 0; i < admins.size(); i++) {
            if (name.equalsIgnoreCase(admins.get(i).getAcName())) {
                theAc = admins.get(i);
                return true;
            }
        }
        return false;
    }

    public boolean passChecker(String password) {
        for (int i = 0; i < accounts.size(); i++)
            if (password.equals(theAc.getPassword())) {
                return true;
            }
        return false;
    }

    public boolean adminPassChecker(String password) {
        for (int i = 0; i < admins.size(); i++)
            if (password.equals(theAc.getPassword())) {
                return true;
            }
        return false;
    }

    private UserAccount userAcParser(String text) {
        String acName = "";
        String password = "";
        double balance = 0.00;

        acName = text.substring(text.indexOf(":") + 2, text.indexOf("Password:") - 1);
        password = text.substring(text.indexOf("Password:") + 10, text.indexOf("balance:"));
        balance = Double.parseDouble(text.substring(text.indexOf("balance:") + 8, text.indexOf("END:")));
        password = password.replace(" ", "");
        return new UserAccount(acName, password, balance);
    }

    private AdminAccount adminAcParser(String text) {
        String acName = "";
        String password = "";
        acName = text.substring(text.indexOf(":") + 2, text.indexOf("Password:") - 1);
        password = text.substring(text.indexOf("Password:") + 10, text.indexOf("END:"));
        password = password.replace(" ", "");
        return new AdminAccount(acName, password);
    }

    public Product nextProduct(int i) {
        return product.get(i);
    }

    private Product productAdder(String line) {
        double cost = 0.00;
        String productName = "";
        int quantity = 0;
        test = line.replace(" ", "");
        productName = test.substring(0, test.indexOf(","));
        cost = Double.parseDouble(test.substring(test.indexOf(",") + 1, test.lastIndexOf(",")));
        quantity = Integer.parseInt(test.substring(test.lastIndexOf(",") + 1, test.indexOf("END")));
        return new Product(productName, cost, quantity);
    }

    // this is called from the vending menu when a purchase is made//
    public boolean productPurchase(int selection) {
        if (selection > product.size() || selection < 0) {
          //  System.out.println("please enter a location number from the selection");
            return false;
        }
        if (prodQuantityReduce(selection, 1) && (acBalanceChecker() + .01) > productPrice(selection)) {
            double replacementBalance = (((UserAccount) theAc).getBalance() + .001) - product.get(selection).getPrice();
            return writeToBalance(replacementBalance);
        } else
            return false;
    }

    public String balanceAsString() {
        return ((UserAccount) theAc).getBalanceAsString();
    }

    public double acBalanceChecker() {
        return ((UserAccount) theAc).getBalance();
    }

    public double productPrice(int selection) {
        return product.get(selection).getPrice();
    }

    public void balanceSetter(double amount) {
        ((UserAccount) theAc).setBalance(amount);
    }

    public boolean writeToFile(String theFile, String stringBuffer) {
        return handler.fileWriter(theFile, stringBuffer);
    }

    public boolean writeToBalance(double replacementBalance) {
        int startIndex = stringBufferOfData.indexOf(theAc.toString());
        int endIndex = startIndex + theAc.toString().length();
        balanceSetter(replacementBalance);
        stringBufferOfData.replace(startIndex, endIndex, theAc.toString());
        return writeToFile(theFile2, stringBufferOfData.toString());
    }

    public boolean writeToQuantity(int replacementQuantity, Product selection) {
        int startIndex = stringBufferOfProducts.indexOf(selection.toString());
        int endIndex = startIndex + selection.toString().length();
        selection.setQuantity(replacementQuantity);
        stringBufferOfProducts.replace(startIndex, endIndex, selection.toString());
        return writeToFile(theFile1, stringBufferOfProducts.toString());
    }


    private boolean prodQuantityReduce(int selection, int amount) {
        for (int i = 0; i < product.size(); i++) {
            if (product.get(i).equals(product.get(selection))) {
                int quantity = product.get(i).getQuantity();
                if (quantity == 0)
                    addToEmptyList(product.get(i));
                if (quantity < amount) {
                   // System.out.println("not enough " + product.get(i).getDescription() + " in stock to fulfill this order");
                    return false;
                }
                return (writeToQuantity(quantity - amount, product.get(i)));

            }
        }
        return false;
    }

    public boolean addToEmptyList(Product theProduct) {
        for (int i = 0; i < emptyProduct.size(); i++) {
            if (theProduct.getDescription().equalsIgnoreCase(emptyProduct.get(i).getDescription())) {
                emptyProduct.add(theProduct);
                return true;
            }
        }
        return false;
    }

    public String emptyList() {
        String list = "";
        if (emptyProduct.size() == 0) {
            return "No items are out of stock";
        }
        for (int i = 0; i < emptyProduct.size(); i++) {
            list = list + emptyProduct.get(i).getDescription() + "\n";
        }
        return list;
    }

    public boolean refiller(String selection, int amount) {
        if (selection.equalsIgnoreCase("ALL")) {
            refillAll(amount);
            return true;
        }
        for (int i = 0; i < sizeOfProductList(); i++) {
            if (selection.equalsIgnoreCase(product.get(i).getDescription())) {
                if (product.get(i).getQuantity() + amount > 30) {
                    return false;
                }
                return amountAdder(product.get(i), amount);
            }
        }
        return false;
    }

    private void refillAll(int amount) {
        for (int i = 0; i < sizeOfProductList(); i++) {
            if (product.get(i).getQuantity() <= 0) {
                if (product.get(i).getQuantity() + amount <= 30) {
                    amountAdder(product.get(i), amount);
                }
            }
        }
        emptyProduct.clear();

    }

    private boolean amountAdder(Product selection, int amount) {
        int originalAmount = selection.getQuantity();
        int totalAmount = originalAmount + amount;
        if (originalAmount <= 0) {
            emptyProduct.remove(selection);
        }
        return writeToQuantity(totalAmount, selection);
    }

    public String productNameList() {
        String list = "";
        int count = 0;
        for (int i = 0; i < product.size(); i++) {
            if (product.get(i).getQuantity() <= 0) {
                list = list + " Location " + count + ": " + product.get(i).getDescription() + " OUT OF STOCK! " + "\n";
                count++;
            } else {
                list = list + " Location " + count + ": " + product.get(i).getDescription() + "\n";
                count++;
            }
        }
        return list;
    }

    public String listItemStock() {
        String list = "";
        int count = 0;
        for (int i = 0; i < product.size(); i++) {
            if (product.get(i).getQuantity() <= 0) {
                list = list + " Location " + count + ": " + product.get(i).getDescription() + " OUT OF STOCK! " + "\n";
                count++;
            } else {
                list = list + " Location " + count + ": " + product.get(i).getDescription() + " Quantity: " + product.get(i).getQuantity() + "\n";
                count++;
            }
        }
        return list;
    }

    public String altProductNameList() {
        StringBuffer list = new StringBuffer();
        DecimalFormat format = new DecimalFormat(".00");
        for (int i = 0; i < product.size(); i++) {
            list.append(product.get(i).getDescription() + " price: €" + format.format(product.get(i).getPrice()) + "\n");
        }
        return list.toString();
    }

    public boolean addProduct(String name, double price, int quantity) {
        Product theProduct = new Product(name, price, quantity);
        if (quantity == 0) {
            emptyProduct.add(theProduct);
        }
        product.add(theProduct);
        stringBufferOfProducts.append(theProduct).append(" END").append("\r\n");
        return writeToFile(theFile1, stringBufferOfProducts.toString());
    }
}