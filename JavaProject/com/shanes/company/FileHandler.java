package com.shanes.company;
import java.io.*;
import java.util.ArrayList;

 class FileHandler {

 boolean fileWriter(String theFile, String stringBuffer){
    try{
        BufferedWriter bufwriter = new BufferedWriter(new FileWriter(theFile));
        bufwriter.write(stringBuffer);
        bufwriter.close();
    } catch (Exception e){
        return  false;
    }
     return true;
}

     ArrayList<String> fileReader(String theFile) throws  IOException {
         ArrayList<String> lines = new ArrayList<>();
         //File f = new File(theFile);
         //if (f.isFile()){
         BufferedReader buffer = null;
         try {

            /* InputStream byteStream = this.getClass().getResourceAsStream(theFile);
             buffer = new BufferedReader(new InputStreamReader(byteStream, "UTF8"));*/
             //InputStream is = getClass().getResourceAsStream(theFile);
             FileInputStream is = new FileInputStream(theFile);
             InputStreamReader isr = new InputStreamReader(is);
              buffer = new BufferedReader(isr);
            // StringBuffer sb = new StringBuffer();
             String line;
             while ((line = buffer.readLine()) != null) {
                 lines.add(line);
             }
         } finally {
             if (buffer != null) buffer.close();
         }
         return lines;
     }}
  //  }
    //return null;}}
