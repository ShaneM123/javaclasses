package com.shanes.company;
import com.shanes.company.VendingException;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A menu from the vending machine.
 */
public class VendingMachineMenu {
    private theVendingMachine theVendingMachine;
    private Scanner in;
    DecimalFormat format = new DecimalFormat(".##");
   private  boolean shutdown = false;

    public VendingMachineMenu(theVendingMachine theVendingMachine) {
        this.theVendingMachine = theVendingMachine;
        in = new Scanner(System.in);
    }

    protected void run()
            throws IOException {
        if (startup()) {
            do {
                int passAttempts = 0;
                boolean more = true;
                boolean adminMore = false;

                while (passAttempts < 4) {
                    if (userDetails()) {
                        more = true;
                        passAttempts = 4;
                        if (theVendingMachine.isAdmin()) {
                            more = false;
                            adminMore = true;
                        }
                    } else {
                        more = false;
                        passAttempts++;
                    }
                }
                System.out.println("Welcome " + getName());
                while (more) {
                    System.out.println("S)how prices  P)urchase  B)alance  L)ogout");

                    String command = in.nextLine().toUpperCase();

                    if (command.equals("S")) {
                        System.out.println(listPrices());

                    } else if (command.equals("P")) {
                        try {
                            System.out.println("Select 0-10 from the list \n" + listItems());
                            makePurchase();
                        } catch (VendingException ex) {
                            System.out.println(ex.getMessage());
                        }
                    } else if (command.equals("B")) {
                        System.out.println("Your Balance: " +getStringBalance());
                    } else if (command.equals("L")) {
                        System.out.println("Logging out..");
                        more = false;
                    }
                }

                while (adminMore) {
                    //NEED TO ADD ERROR HANDLING FOR INPUT//
                    System.out.println("I)tems Out of Stock  R)e-fill products  A)dd products  S)hut Down  P)roduct Stock  L)ogout");
                    String command = in.nextLine().toUpperCase();
                    if (command.equals("A")) {
                        System.out.println("Enter the name of the new Item: ");
                        String name = in.nextLine();
                        System.out.println("Enter the price of the new Item: ");
                        double price = in.nextDouble();
                        in.nextLine();
                        System.out.println("Enter the Quantity you wish to add: ");
                        int quantity = in.nextInt();
                        in.nextLine();
                        addProduct(name, price, quantity);
                    }
                    else if (command.equals("S")) {
                        System.out.println("Shutting down");
                        shutdown = true;
                        adminMore = false;
                    }

                    else if (command.equals("P")) {
                        System.out.println("theVendingMachine: \n" + listItemStock());

                    }
                    else if (command.equals("L")) {
                        System.out.println("Logging out..");
                        adminMore = false;
                    }
                    else if (command.equals("R")) {
                        refill();
                        System.out.println("Re-Filled");
                    }
                    else if (command.equals("I")) {
                        System.out.println("Items out of stock: \n");
                        System.out.println(unstockedList());
                    }

                }
            } while (!shutdown);
        }
    }

    private boolean startup() {
        try {
            theVendingMachine.readProducts();
            theVendingMachine.readAccounts();
            theVendingMachine.readAdmin();
            return true;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return false;
        }
    }

    private boolean userDetails() {
        if (userChecker()) {
            if (passwordCheck()) {
                return true;
            } else {
                System.out.println("wrong password try again");
                return false;
            }
        }
        else{
            System.out.println("wrong username try again");
        return false;}
    }

    private boolean userChecker() {
        System.out.println("please enter your user name");
        String userName = in.nextLine();
         if (theVendingMachine.acChecker(userName)){
             return true;
        }
        else return theVendingMachine.acAdminChecker(userName);

    }

    private boolean passwordCheck() {
        System.out.println("please enter your password");
        String password = in.nextLine();
        if(theVendingMachine.passChecker(password)){
            return true;
        }
        return (theVendingMachine.adminPassChecker(password));
    }

    private void addProduct(String name,double price, int quantity){
        theVendingMachine.addProduct(name, price, quantity);
    }


    private String listItems(){
        return theVendingMachine.productNameList();
    }
    private String listItemStock(){
        return theVendingMachine.listItemStock();
    }
    private  String listPrices(){
        return  theVendingMachine.altProductNameList();
    }
    private String unstockedList(){
        return theVendingMachine.emptyList();
    }
    private void makePurchase(){
        try {
            int selection = in.nextInt();
            if (theVendingMachine.productPurchase(selection)) {
                System.out.println("Product purchased, enjoy! \n your Balance: " + getStringBalance());
            } else {
                System.out.println("unable to purchase product");
                in.nextLine();
            }
        } catch (InputMismatchException e) {
            System.out.println("Locations are numbered 0-10, Please only enter a number for the location selection");
            in.nextLine();
        }
    }
    private String getStringBalance(){return theVendingMachine.balanceAsString();}
    private String getName(){
        return theVendingMachine.getTheAc().getAcName();
    }
    private void refill(){
        System.out.println("Type the name of the product you wish to refill or type \"all\" to refill all empty products");
        try{String product = in.nextLine().toUpperCase();
        System.out.println("quantity you wish to refill by: ");
        int amount = in.nextInt();
        in.nextLine();
        theVendingMachine.refiller(product, amount);
            System.out.println("Updated Stock: \n"+listItemStock());}
        catch (InputMismatchException e){
            System.out.println("wrong input type");
            in.nextLine();
        }
    }

}

/*    private Object getChoice(Object[] choices) {
        while (true) {
            char c = 'A';
            for (Object choice : choices) {
                System.out.println(c + ") " + choice);
                c++;
            }
            String input = in.nextLine();
            int n = input.toUpperCase().charAt(0) - 'A';
            if (0 <= n && n < choices.length)
                return choices[n];
        }
    }*/