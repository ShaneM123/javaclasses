package com.shanes.company;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class AddProductGui {

    private theVendingMachine vend;
    public AddProductGui(theVendingMachine vend) {
        this.vend= vend;
    }

    public  void start(Stage window) throws Exception {
        AdminGui adminGui = new AdminGui(vend);
        ProductListGui productListGui = new ProductListGui(vend);
        PurchaseGui purchasegui = new PurchaseGui(vend);
        GridPane pane = new GridPane();
        VendingGui vendgui = new VendingGui();

        window.setTitle("Vending Machine");

        pane.setPadding(new Insets(10,10,10,10));
        pane.setHgap(10);
        pane.setVgap(8);
        pane.setAlignment(Pos.CENTER);

        Label passLabel = new Label("Add Product");
        GridPane.setConstraints(passLabel,1,0);

        SpinnerValueFactory valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,25,1,1);
        Spinner<Integer> amountInput = new Spinner<>();
        amountInput.setValueFactory(valueFactory);
        amountInput.setEditable(true);
        GridPane.setConstraints(amountInput,1,1);

        SpinnerValueFactory<Double> doubleFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0,100,1,.2);
        Spinner<Double> costInput = new Spinner<>();
        costInput.setValueFactory(doubleFactory);

        Label nameLabel = new Label("Product Name: ");
        GridPane.setConstraints(nameLabel, 0,0);
        TextField nameInput = new TextField();
        nameInput.setPromptText("Name");
        GridPane.setConstraints(nameInput,0,1);

        Button addProductButton = new Button ("Add Product");
        GridPane.setConstraints(addProductButton,1,2);

        addProductButton.setOnAction(e-> {
            if(vend.sizeOfProductList()>=11){
                MessageBox.display("Error","all possible locations already have a product");
            }
            else{vend.addProduct(nameInput.getText(),costInput.getValue(),amountInput.getValue());
            MessageBox.display("Success","Product added");
        }});

        Button backButton = new Button("<- Back");
        GridPane.setConstraints(backButton,0,2);

        backButton.setOnAction(e->   {try{
            adminGui.start(window); }
        catch (Exception d){
            System.out.println(d.getMessage());}});

        pane.getChildren().addAll(nameInput,amountInput,costInput,addProductButton,backButton);
        Scene scene = new Scene(pane,400, 250);
        window.setScene(scene);
        window.show();
    }
}

