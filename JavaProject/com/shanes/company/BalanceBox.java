package com.shanes.company;import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class BalanceBox {

    public static void display(double balance){
        Stage window = new Stage();
        VBox layout = new VBox(10);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Balance");
        window.setMinWidth(250);
        Label label = new Label();
        label.setText("Your Balance: "+ balance);

        Button okayButton = new Button("Okay");

        okayButton.setOnAction(event -> {
            window.close();
        });


        layout.getChildren().addAll(label, okayButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene =new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

    }
    public static void display(String text){
        Stage window = new Stage();
        VBox layout = new VBox(10);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Balance");
        window.setMinWidth(250);
        Label label = new Label();
        label.setText(text);

        Button okayButton = new Button("Okay");

        okayButton.setOnAction(event -> {
            window.close();
        });


        layout.getChildren().addAll(label, okayButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene =new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

    }
}