package com.shanes.company;
import com.shanes.company.VendingGui;
import javafx.application.Application;

import java.io.IOException;
import java.util.Scanner;

/**
   This program simulates a vending machine.
*/
public class VendingMachineSimulation {
    public static void main(String[] args){
            Application.launch(VendingGui.class);
}
}
