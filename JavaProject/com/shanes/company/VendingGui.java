package com.shanes.company;
import com.shanes.company.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.geometry.Insets;

public class VendingGui extends Application {
    private theVendingMachine vend;
    public VendingGui(theVendingMachine vend) {
        this.vend= vend;
    }
    public VendingGui() {
        this.vend= new theVendingMachine();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage window) throws Exception {
        vend.readAccounts();
        vend.readAdmin();
        vend.readProducts();
        AdminGui adminGui = new AdminGui(vend);
        window.setTitle("Vending Machine");
        GridPane pane = new GridPane();
        pane.setPadding(new Insets(10,10,10,10));
        pane.setHgap(10);
        pane.setVgap(8);
        pane.setAlignment(Pos.CENTER);
        CheckBox admin = new CheckBox("admin");
        GridPane.setConstraints(admin, 0,2);

        Label passLabel = new Label("password");
        GridPane.setConstraints(passLabel,1,0);

        PasswordField passInput = new PasswordField();
        passInput.setPromptText("Password");
        GridPane.setConstraints(passInput,1,1);

        Label nameLabel = new Label("Username: ");
        GridPane.setConstraints(nameLabel, 0,0);
        TextField nameInput = new TextField();
        nameInput.setPromptText("Username");
        GridPane.setConstraints(nameInput,0,1);
        Button loginButton = new Button ("Log In");
        GridPane.setConstraints(loginButton,1,2);
        pane.getChildren().addAll(nameInput,nameLabel,passInput,passLabel, loginButton,admin);

        Scene scene = new Scene(pane,400, 250);
        window.setOnCloseRequest(e-> { e.consume();
                if(vend.acAdminChecker(nameInput.getText())&& vend.adminPassChecker((passInput.getText()))){
                closeProgram();}
        else MessageBox.display("Shutdown","Only Admin may shutdown the System");}
       );
        window.setScene(scene);
        window.show();
        UserGui usergui = new UserGui(vend);
        loginButton.setOnAction(e -> {
               try{
               if (admin.isSelected()){
                   if(vend.acAdminChecker(nameInput.getText())&& vend.adminPassChecker((passInput.getText()))){
                       adminGui.start(window);
                   }
               }
              else {
                   if (vend.guiAcChecker(nameInput.getText()) && vend.passChecker((passInput.getText()))) {
                       usergui.start(window);
                   }
               }
           }
           catch (Exception d){
               MessageBox.display("Error", "Login Failed");
           }
           }
        );
    }

    protected void closeProgram(){
        Boolean answer= ConfirmBox.display("Confirm", "Are you sure you want to Shutdown?");
        if(answer){
            Platform.exit();}
        }
    }
