package com.shanes.company;
public class VendingException extends RuntimeException
{
   public VendingException(String reason)
   {
      super(reason);
   }
}
