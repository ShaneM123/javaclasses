package com.shanes.company;
import com.shanes.company.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class UserGui {

        private theVendingMachine vend;
        public UserGui(theVendingMachine vend) {
            this.vend= vend;
        }

        public  void start(Stage window) throws Exception {
            ProductListGui productListGui = new ProductListGui(vend);
            PurchaseGui purchasegui = new PurchaseGui(vend);
            GridPane pane = new GridPane();
            VendingGui vendgui = new VendingGui();

            window.setTitle("Vending Machine");

            pane.setPadding(new Insets(10,10,10,10));
            pane.setHgap(10);
            pane.setVgap(8);
            pane.setAlignment(Pos.CENTER);

            Button buyButton = new Button ("Purchase");
            GridPane.setConstraints(buyButton,1,0);
            Button productsButton = new Button ("Product List");
            GridPane.setConstraints(productsButton,2,0);
            Button balanceButton = new Button ("Balance");
            GridPane.setConstraints(balanceButton,3,0);
            Button logoutButton = new Button ("Logout");
            GridPane.setConstraints(logoutButton,4,0);

            logoutButton.setOnAction(e-> {if(ConfirmBox.display("Logout","are you sure you wish to logout?")){
                try{
                    vendgui.start(window); }
                catch (Exception d){
                    System.out.println(d.getMessage());}
            }
            });

            balanceButton.setOnAction(e-> BalanceBox.display("€"+vend.balanceAsString()));


            productsButton.setOnAction(e-> { try{
                productListGui.start(window); }
                catch (Exception d){
                    System.out.println(d.getMessage());}});

            buyButton.setOnAction(e-> {try{
            purchasegui.start(window);}
            catch(Exception c){
                System.out.println(c.getMessage());
            }
            });

            pane.getChildren().addAll(buyButton,productsButton,balanceButton,logoutButton);
            Scene scene = new Scene(pane,400, 250);
            window.setScene(scene);
            window.show();
        }
    }

