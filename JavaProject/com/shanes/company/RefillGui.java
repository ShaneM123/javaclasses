package com.shanes.company;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.List;


public class RefillGui {

    private theVendingMachine vend;
    public RefillGui(theVendingMachine vend) {
        this.vend= vend;
    }

    public  void start(Stage window) throws Exception {
        List<Button> buttonlist = new ArrayList<>(); //our Collection to hold newly created Buttons
        window.setTitle("Product Stock");
        GridPane pane = new GridPane();
        RefillGui refillGui = new RefillGui(vend);
        pane.setPadding(new Insets(10,10,10,10));
        pane.setHgap(10);
        pane.setVgap(8);
        pane.setAlignment(Pos.CENTER);
        int row= 0;
        for(int i=0; i<vend.sizeOfProductList(); i++) {
            row += 1;
            Product product = vend.nextProduct(i);
            String text = product.getDescription()+" Stock:"+product.getQuantity();
            if (product.getQuantity()<=0){
                text= product.getDescription()+ "\n OUT OF STOCK";
            }
            buttonlist.add(new Button(text));
            buttonlist.get(i).setWrapText(true);
            GridPane.setConstraints(buttonlist.get(i),0,row,2,1);
            pane.getChildren().add(buttonlist.get(i));
            final int j=i;
            buttonlist.get(i).setOnAction(e-> {
                if(vend.nextProduct(j).getQuantity()>=30){
                    MessageBox.display("Error", "Location already full");
                }
                int addedStock= StockerGui.display();
                if (addedStock<=0){MessageBox.display("Error", "Re-Fill Failed \n Please only enter digits");}
               else {if(vend.refiller(vend.nextProduct(j).getDescription(),addedStock)) {
                    MessageBox.display("Success", "Re-fill successful");
                   try{ refillGui.start(window);}
                            catch (Exception d){
                        System.out.println(d.getMessage());}
                }
                else MessageBox.display("Error", "Re-Fill Failed \n max 30 items per location");
                }
            });
        }

        Button backButton = new Button ("<-Main Menu");
        GridPane.setConstraints(backButton,0,0,2,1);
        AdminGui adminGui = new AdminGui(vend);

        backButton.setOnAction(e->   {try{
            adminGui.start(window); }
        catch (Exception d){
            System.out.println(d.getMessage());}});

        pane.getChildren().add(backButton);
        Scene scene = new Scene(pane,600, 400);
        window.setScene(scene);
        window.show();
    }
}

