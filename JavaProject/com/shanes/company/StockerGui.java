package com.shanes.company;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.InputMismatchException;


public class StockerGui {
    public static int display() {
        Stage window = new Stage();
        VBox layout = new VBox(10);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Re-Fill");
        window.setMinWidth(300);
        window.setMaxHeight(200);
        Label label = new Label();
        label.setText("Refill Amount");
        Button setButton = new Button("Set");
        SpinnerValueFactory valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 30, 1, 1);
        Spinner<Integer> amountInput = new Spinner<>();
        try {amountInput.setValueFactory(valueFactory);}
        catch (NumberFormatException e){
            return 0;
        }
        amountInput.setEditable(false);

        GridPane.setConstraints(label, 1, 0);
        GridPane.setConstraints(amountInput, 1, 1);
        GridPane.setConstraints(setButton, 1, 2);

        setButton.setOnAction(event -> {
            window.close();
        });

        layout.getChildren().addAll(label, setButton, amountInput);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
try{        window.showAndWait();}
catch (NumberFormatException e){
    return 0;
}
        return amountInput.getValue();
    }
}