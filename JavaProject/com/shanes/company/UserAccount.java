package com.shanes.company;
import com.shanes.company.Account;

public class UserAccount extends Account {

    private double balance;

    public UserAccount(double balance) {
        this.balance = balance;
    }

    public UserAccount(String acName, String password, double balance) {
        super(acName, password);
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getBalanceAsString() {
        return format.format(balance);
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Name: " + getAcName() + " Password: "+ getPassword()+" balance: "+getBalanceAsString()+" END";
    }
}