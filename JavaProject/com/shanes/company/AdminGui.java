package com.shanes.company;
import com.shanes.company.AddProductGui;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class AdminGui {

    private theVendingMachine vend;
    public AdminGui(theVendingMachine vend) {
        this.vend= vend;
    }

    public  void start(Stage window) throws Exception {
        AdminProductListGui productListGui = new AdminProductListGui(vend);
        RefillGui refillGui = new RefillGui(vend);
        VendingMachineMenu menu = new VendingMachineMenu(vend);
        GridPane pane = new GridPane();
        VendingGui vendgui = new VendingGui();
        AddProductGui addProductGui = new AddProductGui(vend);
        window.setTitle("Vending Machine");

        pane.setPadding(new Insets(10,10,10,10));
        pane.setHgap(10);
        pane.setVgap(8);
        pane.setAlignment(Pos.CENTER);

        Button addProductButton = new Button ("Add Product");
        GridPane.setConstraints(addProductButton,1,0);
        Button refillButton = new Button ("Re-Fill Stock");
        GridPane.setConstraints(refillButton,2,0);
        Button productsButton = new Button ("Product List");
        GridPane.setConstraints(productsButton,3,0);

        Button shutDownButton = new Button ("Shutdown");
        GridPane.setConstraints(shutDownButton,2,1,1,2);
        Button logoutButton = new Button ("Logout");
        GridPane.setConstraints(logoutButton,1,1);
        Button cmdButton = new Button ("Command Line");
        GridPane.setConstraints(cmdButton,3,1);

        logoutButton.setOnAction(e-> {if(ConfirmBox.display("Logout","are you sure you wish to logout?")){
            try{
                vendgui.start(window); }
            catch (Exception d){
                System.out.println(d.getMessage());}
        }
        });

        shutDownButton.setOnAction(e-> vendgui.closeProgram());

        cmdButton.setOnAction(e-> {
            try{
                menu.run();
            }
            catch (Exception d){
                System.out.println(d.getMessage());}});

        productsButton.setOnAction(e-> { try{
            productListGui.start(window); }
        catch (Exception d){
            System.out.println(d.getMessage());}});

        addProductButton.setOnAction(e-> {try{
            addProductGui.start(window);}
        catch(Exception c){
            System.out.println(c.getMessage());
        }
        });
        refillButton.setOnAction(e-> {try{
            refillGui.start(window);}
        catch(Exception c){
            System.out.println(c.getMessage());
        }
        });


        pane.getChildren().addAll(addProductButton,productsButton,shutDownButton,logoutButton,refillButton,cmdButton);
        Scene scene = new Scene(pane,400, 250);
        window.setScene(scene);
        window.show();
    }
}

