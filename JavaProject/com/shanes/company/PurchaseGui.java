package com.shanes.company;
import com.shanes.company.BalanceBox;
import com.shanes.company.Product;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;


public class PurchaseGui {

    private theVendingMachine vend;
    public PurchaseGui(theVendingMachine vend) {
        this.vend= vend;
    }

    public  void start(Stage window) throws Exception {
        List<Button> buttonlist = new ArrayList<>(); //our Collection to hold newly created Buttons
        window.setTitle("Vending Machine");
        GridPane pane = new GridPane();

        pane.setPadding(new Insets(10,10,10,10));
        pane.setHgap(10);
        pane.setVgap(8);
        pane.setAlignment(Pos.CENTER);
        int row= 1;
        for(int i=0; i<vend.sizeOfProductList(); i++) {
             row += 1;
            Product product = vend.nextProduct(i);
            String text = product.getDescription()+" €"+product.getPrice();
            if (product.getQuantity()<=0){
                text= product.getDescription()+ "\n OUT OF STOCK";
            }
            buttonlist.add(new Button(text));
            buttonlist.get(i).setWrapText(true);
            GridPane.setConstraints(buttonlist.get(i),0,row);
            pane.getChildren().add(buttonlist.get(i));
            final int j=i;
            buttonlist.get(i).setOnAction(e-> {
                    if(vend.productPurchase(j)) {
                        BalanceBox.display("Purchase Successful! \n your new Balance: €"+vend.balanceAsString());
                    }
                    else BalanceBox.display("Unable to Purchase Product");
            });
        }

        Button backButton = new Button ("<-Main Menu");
        GridPane.setConstraints(backButton,1,2);
        Button buyButton = new Button ("Purchase->");
        GridPane.setConstraints(buyButton,2,2);
        UserGui usergui = new UserGui(vend);

        backButton.setOnAction(e->   {try{
            usergui.start(window); }
        catch (Exception d){
            System.out.println(d.getMessage());}});

        pane.getChildren().add(backButton);
        Scene scene = new Scene(pane,600, 400);
        window.setScene(scene);
        window.show();
    }
}

