package test.project;
import com.shanes.company.Account;
import com.shanes.company.UserAccount;
import com.shanes.company.theVendingMachine;
import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class theVendingMachineTest extends TestCase {
    public ArrayList<Account> expected = new ArrayList<>();

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }
    @Test
    void testReadAccounts() throws Exception {
        UserAccount account = new UserAccount("Dave Fanning",  "123password", 5.72);
        UserAccount account3 = new UserAccount("Sam kasper",  "17passworder", 3.44);
        expected.add(account);
        expected.add(account3);
        theVendingMachine results = new theVendingMachine();
        results.readAccounts();
        String result = results.getAccounts().toString();
        String expect = expected.toString();

        assertEquals(expect, result);

    }

}