import javax.swing.JOptionPane;
public class main{
	public static void main(String[] args) {
        double annualInterestRate, loanAmount;
        int numberOfYears;
		String strInterest="12.5%";
        try {
            loanAmount = Double.parseDouble(JOptionPane.showInputDialog(null, "Please enter loan amount"));
            annualInterestRate = Double.parseDouble(JOptionPane.showInputDialog(null, "Please enter annual interest amount"));
            numberOfYears = Integer.parseInt(JOptionPane.showInputDialog(null, "Please enter the number of years of repayment"));
            Loan myLoan = new Loan(annualInterestRate, numberOfYears, loanAmount);
			Loan testLoan = new Loan(strInterest);
            JOptionPane.showMessageDialog(null, "Total amount to be repaid: " + myLoan.getTotalPayment());
        } catch (IndexOutOfBoundsException  | NumberFormatException anError )
        {
            JOptionPane.showMessageDialog(null, "Error" + anError.getMessage());
        }
    }

}