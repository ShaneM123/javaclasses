public class Card {
    private String suit;
    private String value;
	private int suitInt;
	private int valueInt;



    public Card(int suitInt, int valueInt) {
        String[] theSuits = {"spades", "hearts", "clubs", "diamonds"};
        String[] theValues = {"ace", "queen", "king", "jack", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
        this.suit = theSuits[suitInt];
        this.value = theValues[valueInt];
		this.suitInt = suitInt;
		this.valueInt = valueInt;
    }

    public String getSuit() {
        return suit;
    }

    public String getValue() {
        return value;
    }
	    public int getSuitInt() {
        return suitInt;
    }

    public int getValueInt() {
        return valueInt;
    }

    @Override
    public String toString() {
        return value + " of "+ suit;
    }
}

