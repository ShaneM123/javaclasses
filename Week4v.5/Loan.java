/*
  Author: Shane Moloney 18119646
  Description: This is a loan class. It should be able to calculate monthly repayments and save details of a loan.
 */
import java.time.*;

public class Loan{
	private double annualInterestRate;
	private int numberOfYears;
	private double loanAmount;
	LocalDate loanDate= LocalDate.now();
	
	//this is the default constructor for the loan class//
	public  Loan(){
		this.annualInterestRate= 2.5;
		this.numberOfYears= 1;
		this.loanAmount = 1000;
		this.loanDate= LocalDate.now();
	}
	
	public Loan(double annualInterestRate, int numberOfYears, double loanAmount){
		this.annualInterestRate = annualInterestRate;
		this.numberOfYears = numberOfYears;
		this.loanAmount = loanAmount;
	}
	//this constructor handles annualInterestRates entered as Strings with percentage signs and converts them to a double// 
	public Loan(String strAnnualInterestRate){
		strAnnualInterestRate = strAnnualInterestRate.replace("%","");
		this.annualInterestRate= Double.valueOf(strAnnualInterestRate).doubleValue();
		this.numberOfYears= 1;
		this.loanAmount = 1000;
		this.loanDate= LocalDate.now();
	}

		public void setAnnualInterestRate(double annualInterestRate){
		this.annualInterestRate=annualInterestRate;
	}
	
	public void setNumberOfYears(int numberOfYears){
		this.numberOfYears= numberOfYears;
	}
	
	public void setLoanAmount(double loanAmount){
		this.loanAmount=loanAmount;
	}
	
	public void setLoanDate(LocalDate loanDate){
		this.loanDate=loanDate;
	}
	
	public double getAnnualInterestRate(){
		return annualInterestRate;
	}
	public int getNumberOfYears(){
		return numberOfYears;
	}
	public double getLoanAmount(){
		return loanAmount;
	}
	public LocalDate getLoanDate(){
		return loanDate;
	}
	
	
	public static void main(String[] args){
		Loan myLoan = new Loan();
	System.out.println(myLoan.getLoanDate());}
	
	
	//this method gets the total payment to be repaid//
	public double getTotalPayment(){
		double totalPayment = loanAmount;
		for(int i=0; i<numberOfYears; i++){
			double totalInterest = (1+ totalPayment/100*annualInterestRate)-1;
			totalPayment= totalPayment+totalInterest;
		}
		return totalPayment;
	}
	//this method tells you the monthly payment to be paid//
	public double getMonthlyPayment(){
		double monthlyPayment= getTotalPayment()/(numberOfYears*12);
		return monthlyPayment;
	}
	
	
}