    import java.util.Arrays;
	import javax.swing.JOptionPane;
public class PokerDriver{
	
	public static void main(String[] args){
        Card[] theHand;
        Card[] theHandTwo;
		int scoreOne, scoreTwo;

        theDeck myDeck = new theDeck();
		myDeck.fillDeck();
        theHand=myDeck.createHand(5);
        theHandTwo=myDeck.createHand(5);
        int handSelected = Integer.parseInt(JOptionPane.showInputDialog(null, "would you like to select hand 1? or hand 2? please type either '1' or '2'to select"));
		JOptionPane.showMessageDialog(null, showHands(theHand,theHandTwo));
		JOptionPane.showMessageDialog(null, compareHands(theHand,theHandTwo));
		}
		
		public static String showHands(Card[] ar1, Card[] ar2){
			String ar1S="", ar2S="";
			
			for(int i=0; i<ar1.length; i++ ){
				ar1S= ar1S + ar1[i].toString();
			}
			
			for(int i=0; i<ar2.length; i++ ){
				ar2S = ar2S + ar2[i].toString();
			}
			return "HAND ONE: "+ar1S+" HAND TWO: " +ar2S ;
		}
		
		//this method sorts the hand dealt by its suit in order//
		public static Card[] handSuitSorter(Card[] array){
   Card temp;
   for (int i = 1; i < array.length; i++) {
    for (int j = i; j > 0; j--) {
     if (array[j].getSuitInt() < array[j - 1].getSuitInt()) {
      temp = array[j];
      array[j] = array[j - 1];
      array[j - 1] = temp;
       };
      }
    }
		return array;
		}
		
		//this method sorts the hand by the value of the card, e.g kind, jack queen etc...//
		public static Card[] handValueSorter(Card[] array){
		 Card temp;
   for (int i = 1; i < array.length; i++) {
    for (int j = i; j > 0; j--) {
     if (array[j].getValueInt() < array[j - 1].getValueInt()) {
      temp = array[j];
      array[j] = array[j - 1];
      array[j - 1] = temp;
       };
	}
   }
   	return array;
	}

		//this method retruns true if the hand is a flush//
	public static boolean flushChecker(Card[] array){
		array= handSuitSorter(array);
	for (int i = 1; i < array.length; i++){
	
		if(array[i].getSuitInt() != array[i-1].getSuitInt()){return false;}
	}
	return true;
	}
	
	//this method retruns true if the hand is a pair//
	public static boolean pairChecker(Card[] array){
		array = handValueSorter(array);
	for (int i = 1; i < array.length; i++){
		
		if(array[i].getValueInt() == array[i-1].getValueInt()){
			return true;
			}
	}
	return false;
	}
	
	//this method retruns true if the hand is a triplet//
		public static boolean tripletChecker(Card[] array){
		array = handValueSorter(array);
	for (int i = 2; i < array.length; i++){
		
		if((array[i].getValueInt()==array[i-1].getValueInt()) && (array[i].getValueInt()==array[i-2].getValueInt())){
			return true;
			}
	}
	return false;
	}
	//this method retruns true if the hand is a straight//
	public static boolean straightChecker(Card[] array){
		array = handValueSorter(array);
		int counter=0;
	for (int i = 1; i < array.length; i++){
	
		if((array[i].getValueInt()-1) ==array[i-1].getValueInt()){
			counter ++;
			}
			if(counter==4){return true;}
	}
	return false;
	}
	//this method compares the two hands, gives each hand a score and then states the winner as a string return//
	public static String compareHands(Card[] arOne, Card[] arTwo){
				 int scOne=0;
				 int scTwo=0;
		String winOne = "first hand is the winner";
		String winTwo = "second hand is the winner";
		
		if(flushChecker(arOne)){ scOne=scOne+20;}
		if(flushChecker(arTwo)){ scTwo=scTwo+20;}
		if(straightChecker(arOne)){ scOne=scOne+15;}
		if(straightChecker(arTwo)){ scTwo=scTwo+15;}
		if(tripletChecker(arOne)){scOne=scOne+10;}
		if(tripletChecker(arTwo)){scTwo=scTwo+10;}
		if(pairChecker(arOne)){scOne= scOne+5;}
		if(pairChecker(arTwo)){scTwo=scTwo+5;}
		

		
		if(scOne>scTwo){
			return winOne;
		}
		else return winTwo;
	}

}