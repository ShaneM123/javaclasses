import java.util.Random;

public class Hand{

    private Random diceRoller = new Random();
	private Card[] theHand = new Card[5];
	private boolean[] deck = new boolean[52];
	private int suitVal;
	private int valueVal;

    private static boolean areAllTrue(boolean[] array)
    {
        for(boolean b : array) if(!b) return false;
        return true;
    }



    Card[] cards = new Card[52];

    public void fillHand(){
        while(!areAllTrue(deck)){
    for (int i=0; i<theHand.length; i++) {
        int randCard = diceRoller.nextInt(52) + 1;
        while (deck[randCard]) {
            randCard = diceRoller.nextInt(52) + 1;
        }
        if (randCard <= 13) {
            suitVal = 1;
            valueVal = randCard;
        } else if (randCard <= 26) {
            suitVal = 2;
            valueVal = randCard - 13;

        } else if (randCard <= 39) {
            suitVal = 3;
            valueVal = randCard - 26;

        } else if (randCard <= 52) {
            suitVal = 4;
            valueVal = randCard - 39;
        }
        theHand[i] = new Card(suitVal, valueVal);
        deck[randCard] = true;

    }
    }
    }






}