/*
* This class builds a deck of cards by initializing each card in a deck via a for loop
*
* */

import java.util.Arrays;
import java.util.Random;

public class theDeck {

    private Random randomGen = new Random();
	private boolean[] deck;     //for each card that is added, this deck is set to true;//
    private Card[] cards;
    private int deckSize;

    //default constructor of a 52 card deck//
    public theDeck() {
        this.deckSize = 52;
		this.deck  = new boolean[deckSize]; 
		this.cards = new Card[deckSize];
    }
    //this constructor lets you select your deck size if you wish//
    public theDeck(int deckSize) {
        if (deckSize>52){
            deckSize=52;
        }
        this.deckSize = deckSize;
		this.deck  = new boolean[deckSize]; 
		this.cards = new Card[deckSize];
    }

    //these two methods test to see if the deck of cards is either empty or full//
    private static boolean areAllTrue(boolean[] array) {
        for (boolean b : array) if (!b) return false;
        return true;
    }
    private static boolean areAllFalse(boolean[] array) {
        for (boolean b : array) if (b) return true;
        return false;
    }

                    //this method fills the deck with 52 playing cards as an array of card objects//
    public void fillDeck() {
         int suitVal;            //the cards suit value//
         int valueVal;          // the cards value in a suit//
        while (!areAllTrue(deck)) {
            for (int i = 0; i < deckSize; i++) {
                int addedCard = randomGen.nextInt(deckSize);
                while(deck[addedCard]){
                    addedCard = randomGen.nextInt(deckSize);
                }
                if (addedCard < 13) {
                    suitVal = 0;
                    valueVal = addedCard;
                } else if (addedCard < 26) {
                    suitVal = 1;
                    valueVal = addedCard - 13;

                } else if (addedCard < 39) {
                    suitVal = 2;
                    valueVal = addedCard - 26;

                } else if (addedCard < 52) {
                    suitVal = 3;
                    valueVal = addedCard - 39;
                }
                else {
                    suitVal= -1;valueVal=-1;
                }
                cards[addedCard] = new Card(suitVal, valueVal);
                deck[addedCard] = true;
            }
        }
    }
                //this method creates a hand of cards in a random fashion, removing each card taken from the deck and then returns the hand//
    public Card[] createHand(int handSize) {
        if (handSize > deck.length) {
            return null;
       }
        Card[] theHand = new Card[handSize];

            for (int i = 0; i < theHand.length; i++) {
                int randCard = randomGen.nextInt(deckSize);
                while(!deck[randCard]){
                     randCard = randomGen.nextInt(deckSize);
                }

         
                theHand[i]=cards[randCard];
                deck[randCard]=false;
            }
    
        return theHand;
    }

    @Override
    public String toString() {
        return 
                " cards=" + Arrays.toString(cards) +
                '}';
    }
}